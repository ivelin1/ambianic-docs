
# An Open Source Smart Camera Project [![GitHub User's stars](https://img.shields.io/github/stars/ambianic?style=social)](https://github.com/ambianic)

[![Join the Slack chat room](https://img.shields.io/badge/Slack-Join%20the%20chat%20room-blue)](https://join.slack.com/t/ambianicai/shared_invite/zt-eosk4tv5-~GR3Sm7ccGbv1R7IEpk7OQ)

Ambianic is an [award winning](https://blog.ambianic.ai/2020/11/05/awards.html) Open Source Smart Camera project optimized for Raspberry Pi 4B. It aims to make our homes and workspaces safer by providing helpful and actionable suggestions. For example, notify family members instantly if a loved one has fallen down.

Ambianic has two major components: 

* [Ambianic Edge](users/ambianicedge): an IoT device with a camera and a built-in AI engine for on-device inference and training. Usually runs within an [Ambianic Box](users/ambianicbox) enclosure.
* [Ambianic UI](users/ambianicui): a front-end Progressive Web App to manage Ambianic Edge devices.

Following is a high level diagram of a typical Ambianic deployment.

![Ambianic High Level Diagram](../assets/images/Ambianic-High-Level-Diagram.png)


[Quick Start Guide](https://docs.ambianic.ai/users/quickstart/) |
[Try it Now](https://ui.ambianic.ai) |
[View on Github](https://github.com/ambianic) |

